import serial

from nmea_msgs.msg import Sentence
import rclpy

from rclpy.node import Node


def main(args=None):
    rclpy.init(args=args)

    driver = Node("nmea_navsat_driver")

    nmea_pub = driver.create_publisher(Sentence, "nmea_sentence", 10)

    serial_port = driver.declare_parameter('port', '/dev/ttyUSB0').value
    serial_baud = driver.declare_parameter('baud', 115200).value

    # Get the frame_id
    frame_id =  "gps"  #driver.get_frame_id()

    try:
        GPS = serial.Serial(port=serial_port, baudrate=serial_baud, timeout=2)
        try:
            while rclpy.ok():
                data = GPS.readline().strip()
                if data.__len__() ==0:
                    continue
                sentence = Sentence()
                sentence.header.stamp = driver.get_clock().now().to_msg()
                sentence.header.frame_id = frame_id
                try:
                    # sentence.sentence = str(data,'utf-8')
                    sentence.sentence = str(data,'ascii')
                    # print(sentence.sentence)
                    nmea_pub.publish(sentence)
                except Exception as e:
                    driver.get_logger().error("Ros error: {0}".format(e))

        except Exception as e:
            driver.get_logger().error("Ros error: {0}".format(e))
            GPS.close()  # Close GPS serial port
    except serial.SerialException as ex:
        driver.get_logger().fatal("Could not open serial port: I/O error({0}): {1}".format(ex.errno, ex.strerror))
