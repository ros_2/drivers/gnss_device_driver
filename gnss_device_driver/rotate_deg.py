def rotate(base_deg, _deg):
    # if _deg  = 180 or deg = -180 :
        # pass
    if _deg >= 0 and _deg < 180:
        _deg = _deg % 180
    if _deg  < 0 and _deg > -180:
        _deg = _deg % -180

    base_deg -= _deg

    vlaue_deg =base_deg 
    if base_deg >= 180:
        vlaue_deg  = -180 + (base_deg%180)
    elif base_deg < -180:
        vlaue_deg = 180 + (base_deg%-180)

    if vlaue_deg == -180:
        vlaue_deg = 180

    return vlaue_deg


def heading2yaw(heading_deg):
    _gnss_yaw = 0
    if heading_deg > 180:
        _gnss_yaw =  360 - heading_deg
    else:
        _gnss_yaw = 0 - heading_deg
    if _gnss_yaw == -180:
        _gnss_yaw = 180
    return _gnss_yaw

if __name__ == "__main__":
    # for _deg in range(-135, 185,45):
    _deg = -90
    for base_deg in range(0,360,45):
        yaw_deg = heading2yaw(base_deg)
        print(_deg," : [", base_deg," ",yaw_deg, "]  => ", rotate(yaw_deg,_deg))
            
    print("----")
    base_deg = 264.97
    yaw_deg = heading2yaw(base_deg)
    print(_deg," : [", base_deg," ",yaw_deg, "]  => ", rotate(yaw_deg,-90))


