#!/usr/bin/env python3
#coding=utf-8

import rclpy
from rclpy.node import Node
import  math

from std_msgs.msg import String
from nmea_msgs.msg import Sentence

from sensor_msgs.msg import Imu
from sensor_msgs.msg import NavSatFix
from gps_msgs.msg import GPSFix  # gps_umd :  https://github.com/swri-robotics/gps_umd/tree/ros2-devel

import tf_transformations

from . import rotate_deg 

class GpsPares_NewtonM2(object):
    """
    ['$GPFPD', '2117', '546559.380', '81.348', '1.461', 
      '0.295', '36.70388968', '117.14939980', '38.02', '0.005', 
      '0.001', '-0.002', '1.366', '22', '24', 
      '4B*2B']
    ["$GTIMU,2133,456124.100,-0.0634,0.7186,0.3897,-0.0228,0.0011,0.9868,31.5*60"]

    """
    def parse( self,msg ):
        info = {}
        tmp_list = msg.split(",")
        try:
            if tmp_list[0] in ["$GPFPD"]:
                if tmp_list[3] == "":
                    print("GpsPares_NewtonM2  parse error !!!")
                    return {}

                info['heading'] = float(tmp_list[3])  # 航向角
                # info['yaw'] = rotate_deg.heading2yaw(info['heading']) 
                info['pitch'] = float(tmp_list[4])
                info['roll'] = float(tmp_list[5])

                info['lat'] = float(tmp_list[6])
                info['lon'] = float(tmp_list[7])
                info['altitude'] = float(tmp_list[8])

                Ve = float(tmp_list[9]) # 东向速度 m/s
                Vn = float(tmp_list[10]) # 北向速度 m/s
                Vu = float(tmp_list[11]) # 天向速度 m/s 

                info['speed'] = ((Ve**2 +Vn**2)**0.5)* 3.6
                # info['cnt'] = int(tmp_list[13][0:2])
                info['cnt'] = int(tmp_list[13]) + int(tmp_list[14])
                info["gps_state"] = 0  
                if tmp_list[15][0:2] == "4B":
                    info["gps_state"] = 4  

                _gpfpd_info = {"type":"GPFPD",
                         "info": info }
                return _gpfpd_info

            if tmp_list[0] in ["$GTIMU"]:
                info["gyro_x"] = float(tmp_list[3]) 
                info["gyro_y"] = float(tmp_list[4]) 
                info["gyro_z"] = float(tmp_list[5]) 

                _acc_x = float(tmp_list[6]) * 9.8
                _acc_y = float(tmp_list[7]) * 9.8
                _acc_z = float(tmp_list[8]) * 9.8 

                info['acc_x'] = _acc_x
                info['acc_y'] = _acc_y
                info['acc_z'] = _acc_z 

                _gtimu_info = {"type":"GTIMU",
                         "info": info }
                return _gtimu_info

            return {"type":None, "info": None }

        except Exception as e:
            print(e)
            return {}

# import math
# import struct
# RAD2DEG = 180/math.pi

# def test_main():
#     gps_pares = GpsPares_NewtonM2()
#     msg = "$GPFPD,2117,546559.380,81.348,1.461,0.295,36.70388968,117.14939980,38.02,0.005,0.001,-0.002,1.366,22,24,4B*2B"
#     ret = gps_pares.parse(msg)
#     print(ret)


class MinimalSubscriber(Node):

    def __init__(self):
        super().__init__('newton_m2_driver')
        self._parse =  GpsPares_NewtonM2()

        self.__info = {}

        self.fix_pub = self.create_publisher(GPSFix, "/gnss/fix", 10)
        self.navsat_fix_pub = self.create_publisher( NavSatFix, "/gnss/navsat_fix", 10)
        self.imu_raw_pub = self.create_publisher( Imu, "/gnss/imu_raw", 10)

        self.d_yaw = self.declare_parameter('d_yaw', -90).value # default -90

        # Sentence, "nmea_sentence",

        self.subscription = self.create_subscription(
            Sentence,
            'nmea_sentence',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        try:
            info = self._parse.parse(msg.sentence)
            # _info = self._parse.parse(msg.sentence)
            # self.get_logger().info('msg.sentence: "%s"' % msg.sentence)
            # self.get_logger().info('GPS : "%s"' % _info)


            if info["type"] == None:
                return

            if info["type"] == "GPFPD":
                self.__info['lat']       = float(info["info"].get('lat', self.__info.get('lat',0.0) ) ) 
                self.__info['lon']       = float(info["info"].get('lon',self.__info.get('lon',0.0))) 
                self.__info['altitude']  = float(info["info"].get('altitude',0.0)) 
                self.__info['speed']     = float(info["info"].get('speed',  self.__info.get('speed',0.0)))
                self.__info['gps_state'] = int(  info["info"].get('gps_state',self.__info.get('gps_state',0.0)))
                self.__info['azimutha']  = float(info["info"].get('heading',self.__info.get('azimutha',0.0)))
                self.__info['cnt']       = float(info["info"].get('cnt',0.0))

                # 旋转
                _yaw =rotate_deg.heading2yaw(self.__info['azimutha'])
                self.__info['yaw'] =rotate_deg.rotate(_yaw,self.d_yaw) 

                self.__info['pitch'] = info["info"]["pitch"]
                self.__info['roll'] = info["info"]["roll"]

                if self.__info['gps_state'] != 4:
                    msg = "error gps_state %s " % self.__info.get('gps_state')
                    self.get_logger().error(msg)

                # publish fix
                fix = GPSFix()
                fix.header.stamp = self.get_clock().now().to_msg()
                fix.header.frame_id = "/gps"#frame_id
                fix.status.status = fix.status.STATUS_NO_FIX
                if self.__info.get('gps_state',0) == 4:
                    fix.status.status = fix.status.STATUS_FIX
                if self.__info.get('gps_state',0) == 5:
                    fix.status.status = fix.status.STATUS_WAAS_FIX

                fix.latitude = self.__info.get('lat',0.0)
                fix.longitude = self.__info.get('lon',0.0)
                fix.altitude = self.__info.get('altitude',0.0)
                fix.track = self.__info.get('azimutha',0.0)
                fix.speed = self.__info.get('speed',0.0) /3.6 
                fix.climb = 0.0  # 垂直速度
                fix.dip   = self.__info.get('yaw',0.0)
                fix.pitch = self.__info.get('pitch',0.0)
                fix.roll  = self.__info.get('roll',0.0)
                self.fix_pub.publish(fix)

                # publish navsat_fix
                navsat_fix = NavSatFix()
                navsat_fix.header.stamp = self.get_clock().now().to_msg()
                navsat_fix.header.frame_id = 'navsat_link'

                navsat_fix.status.status = navsat_fix.status.STATUS_NO_FIX
                if self.__info.get('gps_state',0) == 4:
                    navsat_fix.status.status = navsat_fix.status.STATUS_FIX

                navsat_fix.latitude = self.__info.get('lat',0.0)
                navsat_fix.longitude = self.__info.get('lon',0.0)
                navsat_fix.altitude = self.__info.get('altitude',0.0)

                self.navsat_fix_pub.publish(navsat_fix)


            if info["type"] == "GTIMU":

                # 角速度
                self.__info['gyro_x']    = float(info["info"].get('gyro_x', 0.0))
                self.__info['gyro_y']    = float(info["info"].get('gyro_y', 0.0))
                self.__info['gyro_z']    = float(info["info"].get('gyro_z', 0.0))

                # 加速度
                self.__info['acc_x']     = float(info["info"].get('acc_x', 0.0))
                self.__info['acc_y']     = float(info["info"].get('acc_y', 0.0))
                self.__info['acc_z']     = float(info["info"].get('acc_z', 0.0))

                # publish navsat_fix
                imu = Imu()
                imu.header.stamp = self.get_clock().now().to_msg()
                imu.header.frame_id = 'imu_link'
                

                # http://docs.ros.org/en/galactic/Tutorials/Tf2/Writing-A-Tf2-Broadcaster-Py.html#write-the-broadcaster-node
                quaternion = tf_transformations.quaternion_from_euler(
                                        math.radians(self.__info.get('roll',0.0)),
                                        math.radians(self.__info.get('pitch',0.0)),
                                        math.radians(self.__info.get('yaw',0.0))
                                    )
                # quaternion = tf_transformations.quaternion_from_euler(
                #                         numpy.deg2rad(self.__info.get('roll',0.0)),
                #                         numpy.deg2rad(self.__info.get('pitch',0.0)),
                #                         numpy.deg2rad(self.__info.get('yaw',0.0))
                #                     )
                imu.orientation.x = quaternion[0]
                imu.orientation.y = quaternion[1]
                imu.orientation.z = quaternion[2]
                imu.orientation.w = quaternion[3]

                # #  角速度
                imu.angular_velocity.x = float(self.__info.get('gyro_x',0.0)) 
                imu.angular_velocity.y = float(self.__info.get('gyro_y',0.0)) 
                imu.angular_velocity.z = float(self.__info.get('gyro_z',0.0)) 

                # #  加速度
                imu.linear_acceleration.x = float(self.__info.get('acc_x',0.0))  
                imu.linear_acceleration.y = float(self.__info.get('acc_y',0.0))  
                imu.linear_acceleration.z = float(self.__info.get('acc_z',0.0))  

                self.imu_raw_pub.publish(imu)

        except Exception as e:
            print("err : ",e)


def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = MinimalSubscriber()

    rclpy.spin(minimal_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
